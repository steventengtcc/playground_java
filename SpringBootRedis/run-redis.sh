#!/bin/sh

if [ $1 = "up" ]; then
    echo "Execute Command $1."
    docker-compose -f redis.yml up -d
elif [ $1 = "down" ]; then
    echo "Execute Command $1. Stop & Remove Container."
    docker-compose -f redis.yml down
elif [ $1 = "stop" ]; then
    echo "Execute Command $1."
    docker-compose -f redis.yml stop
else
    echo "Unknown command. Support \"up\", \"down\" and \"stop\" only"
fi
