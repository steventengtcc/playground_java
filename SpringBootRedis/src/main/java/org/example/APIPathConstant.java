package org.example;

public class APIPathConstant {
    public static final String CACHE = "/api/cache";
    public static final String CACHE_MANAGER = "/cacheManager";
    public static final String REDIS_TEMPLATE = "/redisTemplate";

    private APIPathConstant() {
    }
}
