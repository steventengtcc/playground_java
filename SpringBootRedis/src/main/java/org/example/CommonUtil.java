package org.example;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.UUID;

public class CommonUtil {

    public static String genResult(String key) {
        final var currentTime = LocalDateTime.now().toString();
        final var uuid = UUID.randomUUID();
        return MessageFormat.format("{0}, {1}, {2}", uuid.toString(), currentTime, key);
    }

    public static String genRedisTemplateKey(String prefix, String key) {
        return MessageFormat.format("{0}::{1}", prefix, key);
    }
}
