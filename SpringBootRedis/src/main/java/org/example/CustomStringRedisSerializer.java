package org.example;

import java.util.Objects;
import java.util.Optional;

import org.springframework.data.redis.serializer.StringRedisSerializer;

public class CustomStringRedisSerializer extends StringRedisSerializer {

    private final String prefix;

    private CustomStringRedisSerializer(String prefix) {
        Objects.requireNonNull(prefix, "prefix must not be null");
        this.prefix = prefix;
    }

    public static CustomStringRedisSerializer of(String prefix) {
        return new CustomStringRedisSerializer(prefix);
    }

    @Override
    public String deserialize(byte[] bytes) {
        return Optional.ofNullable(super.deserialize(bytes))
                .map(s -> s.substring(prefix.length()))
                .orElse(null);
    }

    @Override
    public byte[] serialize(String string) {
        return Optional.ofNullable(string)
                .map(s -> prefix + s)
                .map(super::serialize)
                .orElse(null);
    }
}
