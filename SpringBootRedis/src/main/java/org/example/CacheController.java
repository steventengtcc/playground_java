package org.example;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(APIPathConstant.CACHE)
@RequiredArgsConstructor
public class CacheController {

    private final CacheService cacheService;

    @PutMapping("/{key}")
    public ResponseEntity<String> updateCache(@PathVariable("key") final String key) {
        return ResponseEntity.ok().body(cacheService.updateCache(key));
    }

    @GetMapping("/{key}")
    public ResponseEntity<String> loadCache(@PathVariable("key") final String key) {
        return ResponseEntity.ok().body(cacheService.loadCache(key));
    }

    @DeleteMapping("/{key}")
    public ResponseEntity<?> clearCache(@PathVariable("key") final String key) {
        cacheService.clearCache(key);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping
    public ResponseEntity<?> clearCache() {
        cacheService.clearCache();
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(APIPathConstant.CACHE_MANAGER + "/{key}")
    public ResponseEntity<?> clearCacheByCacheManager(@PathVariable("key") final String key) {
        cacheService.clearCacheByCacheManager(key);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(APIPathConstant.CACHE_MANAGER)
    public ResponseEntity<?> clearCacheByCacheManager() {
        cacheService.clearCacheByCacheManager();
        return ResponseEntity.noContent().build();
    }

    @PutMapping(APIPathConstant.REDIS_TEMPLATE + "/{key}")
    public ResponseEntity<String> updateCacheByRedisTemplate(@PathVariable("key") final String key) {
        return ResponseEntity.ok().body(cacheService.updateCacheByRedisTemplate(key));
    }

    @GetMapping(APIPathConstant.REDIS_TEMPLATE + "/{key}")
    public ResponseEntity<String> loadCacheByRedisTemplate(@PathVariable("key") final String key) {
        return ResponseEntity.ok().body(cacheService.loadCacheByRedisTemplate(key));
    }

    @DeleteMapping(APIPathConstant.REDIS_TEMPLATE + "/{key}")
    public ResponseEntity<?> clearCacheByRedisTemplate(@PathVariable("key") final String key) {
        cacheService.clearCacheByRedisTemplate(key);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(APIPathConstant.REDIS_TEMPLATE)
    public ResponseEntity<?> clearCacheByRedisTemplate() {
        cacheService.clearCacheByRedisTemplate();
        return ResponseEntity.noContent().build();
    }
}
