package org.example;

import java.util.Optional;
import java.util.Set;

import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CacheService {

    private static final String CACHE_NAME = CacheConstant.CACHE_NAME;

    private final CacheManager cacheManager;
    private final StringRedisTemplate redisTemplate;

    @CachePut(cacheNames = CACHE_NAME)
    public String updateCache(String key) {
        return CommonUtil.genResult(key);
    }

    @Cacheable(cacheNames = CACHE_NAME)
    public String loadCache(String key) {
        return CommonUtil.genResult(key);
    }

    @CacheEvict(cacheNames = CACHE_NAME)
    public void clearCache(String key) {
    }

    @CacheEvict(cacheNames = CACHE_NAME, allEntries = true)
    public void clearCache() {
    }

    public void clearCacheByCacheManager(String key) {
        Optional.ofNullable(cacheManager.getCache(CACHE_NAME))
                .ifPresent(cache -> cache.evict(key));
    }

    public void clearCacheByCacheManager() {
        Optional.ofNullable(cacheManager.getCache(CACHE_NAME))
                .ifPresent(Cache::clear);
    }

    public String updateCacheByRedisTemplate(String key) {
        final var result = CommonUtil.genResult(key);
        redisTemplate.opsForValue().set(CommonUtil.genRedisTemplateKey(CacheConstant.CACHE_NAME, key), result);
        return result;
    }

    public String loadCacheByRedisTemplate(String key) {
        final var redisTemplateKey = CommonUtil.genRedisTemplateKey(CacheConstant.CACHE_NAME, key);
        if (Boolean.TRUE.equals(redisTemplate.hasKey(redisTemplateKey))) {
            return redisTemplate.opsForValue().get(redisTemplateKey);
        }
        return updateCacheByRedisTemplate(key);
    }

    public void clearCacheByRedisTemplate(String key) {
        redisTemplate.delete(CommonUtil.genRedisTemplateKey(CacheConstant.CACHE_NAME, key));
    }

    public void clearCacheByRedisTemplate() {
        final var keySet = Optional.ofNullable(redisTemplate.keys(CommonUtil.genRedisTemplateKey(CacheConstant.CACHE_NAME, "*")))
                .orElseGet(Set::of);
        redisTemplate.delete(keySet);
    }
}
