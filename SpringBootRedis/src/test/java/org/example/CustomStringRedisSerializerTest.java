package org.example;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class CustomStringRedisSerializerTest {

    @Test
    void testSerDeserialize() {
        final var customStringRedisSerializer = CustomStringRedisSerializer.of("prefix");
        final var key = "key";

        final var serializedKey = customStringRedisSerializer.serialize(key);
        final var deserializedKey = customStringRedisSerializer.deserialize(serializedKey);
        assertEquals(key, deserializedKey);
    }
}
