package integration_test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.example.Main;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import testing_util.ApiUtil;
import testing_util.MockMvcUtil;

@SpringBootTest(classes = Main.class)
@AutoConfigureMockMvc
class CacheTest {

    @Autowired
    CacheTest(MockMvc mockMvc) {
        MockMvcUtil.setMockMvc(mockMvc);
    }

    /**
     * Test to verify that loading the cache with the same key
     * returns the same value.
     */
    @Test
    void createCacheWithLoadApi() throws Exception {
        final var result1 = ApiUtil.loadCache("mockKey", status().isOk());
        final var result2 = ApiUtil.loadCache("mockKey", status().isOk());
        assertEquals(result1, result2);

        final var result3 = ApiUtil.loadCache("mockKey1", status().isOk());
        final var result4 = ApiUtil.loadCache("mockKey1", status().isOk());
        assertEquals(result3, result4);
        assertNotEquals(result1, result3);
    }

    /**
     * Test to verify that updating the cache with a specific key
     * returns the updated value when the cache is loaded.
     */
    @Test
    void updateCache() throws Exception {
        final var result1 = ApiUtil.updateCache("mockKey", status().isOk());
        final var result2 = ApiUtil.loadCache("mockKey", status().isOk());
        assertEquals(result1, result2);

        final var result3 = ApiUtil.updateCache("mockKey1", status().isOk());
        final var result4 = ApiUtil.loadCache("mockKey1", status().isOk());
        assertEquals(result3, result4);
        assertNotEquals(result1, result3);
    }

    /**
     * Test to verify that delete the cache with a specific key
     * Deletes the cache with the key and returns the new value when the cache is loaded.
     */
    @Test
    void deleteCache() throws Exception {
        final var result1 = ApiUtil.loadCache("mockKey", status().isOk());
        final var result2 = ApiUtil.loadCache("mockKey1", status().isOk());
        ApiUtil.deleteCache("mockKey", status().isNoContent());

        final var result3 = ApiUtil.loadCache("mockKey", status().isOk());
        final var result4 = ApiUtil.loadCache("mockKey1", status().isOk());
        assertNotEquals(result1, result3);
        assertEquals(result2, result4);
    }

    /**
     * Test to verify that delete the cache without key
     * Deletes all key and returns the new value when the cache is loaded.
     */
    @Test
    void deleteAllCache() throws Exception {
        final var result1 = ApiUtil.loadCache("mockKey", status().isOk());
        final var result2 = ApiUtil.loadCache("mockKey1", status().isOk());
        ApiUtil.deleteCache(status().isNoContent());

        final var result3 = ApiUtil.loadCache("mockKey", status().isOk());
        final var result4 = ApiUtil.loadCache("mockKey1", status().isOk());
        assertNotEquals(result1, result3);
        assertNotEquals(result2, result4);
    }

    /**
     * Test to verify that delete the cache with a specific key via cache manager.
     * Deletes the cache with the key and returns the new value when the cache is loaded.
     */
    @Test
    void deleteCacheByCacheManager() throws Exception {
        final var result1 = ApiUtil.loadCache("mockKey", status().isOk());
        final var result2 = ApiUtil.loadCache("mockKey1", status().isOk());
        ApiUtil.deleteCacheByCacheManager("mockKey", status().isNoContent());

        final var result3 = ApiUtil.loadCache("mockKey", status().isOk());
        final var result4 = ApiUtil.loadCache("mockKey1", status().isOk());
        assertNotEquals(result1, result3);
        assertEquals(result2, result4);
    }

    /**
     * Test to verify that delete the cache without key via cache manager.
     * Deletes all key and returns the new value when the cache is loaded.
     */
    @Test
    void deleteAllCacheByCacheManager() throws Exception {
        final var result1 = ApiUtil.loadCache("mockKey", status().isOk());
        final var result2 = ApiUtil.loadCache("mockKey1", status().isOk());
        ApiUtil.deleteCacheByCacheManager(status().isNoContent());

        final var result3 = ApiUtil.loadCache("mockKey", status().isOk());
        final var result4 = ApiUtil.loadCache("mockKey1", status().isOk());
        assertNotEquals(result1, result3);
        assertNotEquals(result2, result4);
    }

    @Test
    void createCacheByRedisTemplate() throws Exception {
        final var result1 = ApiUtil.loadCacheByRedisTemplate("mockKey", status().isOk());
        final var result2 = ApiUtil.loadCacheByRedisTemplate("mockKey", status().isOk());
        assertEquals(result1, result2);

        final var result3 = ApiUtil.loadCacheByRedisTemplate("mockKey1", status().isOk());
        final var result4 = ApiUtil.loadCacheByRedisTemplate("mockKey1", status().isOk());
        assertEquals(result3, result4);
        assertNotEquals(result1, result3);
    }

    @Test
    void updateCacheByRedisTemplate() throws Exception {
        final var result1 = ApiUtil.updateCacheByRedisTemplate("mockKey", status().isOk());
        final var result2 = ApiUtil.loadCacheByRedisTemplate("mockKey", status().isOk());
        assertEquals(result1, result2);

        final var result3 = ApiUtil.updateCacheByRedisTemplate("mockKey1", status().isOk());
        final var result4 = ApiUtil.loadCacheByRedisTemplate("mockKey1", status().isOk());
        assertEquals(result3, result4);
        assertNotEquals(result1, result3);
    }

    @Test
    void deleteCacheByRedisTemplate() throws Exception {
        final var result1 = ApiUtil.loadCacheByRedisTemplate("mockKey", status().isOk());
        final var result2 = ApiUtil.loadCacheByRedisTemplate("mockKey1", status().isOk());
        ApiUtil.deleteCacheByRedisTemplate("mockKey", status().isNoContent());

        final var result3 = ApiUtil.loadCacheByRedisTemplate("mockKey", status().isOk());
        final var result4 = ApiUtil.loadCacheByRedisTemplate("mockKey1", status().isOk());
        assertNotEquals(result1, result3);
        assertEquals(result2, result4);
    }

    @Test
    void deleteAllCacheByRedisTemplate() throws Exception {
        final var result1 = ApiUtil.loadCacheByRedisTemplate("mockKey", status().isOk());
        final var result2 = ApiUtil.loadCacheByRedisTemplate("mockKey1", status().isOk());
        ApiUtil.deleteCacheByRedisTemplate(status().isNoContent());

        final var result3 = ApiUtil.loadCacheByRedisTemplate("mockKey", status().isOk());
        final var result4 = ApiUtil.loadCacheByRedisTemplate("mockKey1", status().isOk());
        assertNotEquals(result1, result3);
        assertNotEquals(result2, result4);
    }

    /**
     * Test to verify that create the cache with spring boot annotations.
     * Deletes the cache by CacheManager / RedisTemplate.
     */
    @Test
    void createAndDeleteCacheByDifferentWay() throws Exception {
        final var result1 = ApiUtil.loadCache("mockKey", status().isOk());
        final var result2 = ApiUtil.loadCache("mockKey1", status().isOk());
        ApiUtil.deleteCacheByCacheManager("mockKey", status().isNoContent());
        ApiUtil.deleteCacheByRedisTemplate("mockKey1", status().isNoContent());

        final var result3 = ApiUtil.loadCache("mockKey", status().isOk());
        final var result4 = ApiUtil.loadCache("mockKey1", status().isOk());
        assertNotEquals(result1, result3);
        assertNotEquals(result2, result4);
    }
}
