package testing_util;

import org.springframework.test.web.servlet.MockMvc;

import lombok.Getter;

public class MockMvcUtil {

    @Getter
    private static MockMvc mockMvc;

    private MockMvcUtil() {
    }

    public static void setMockMvc(MockMvc mockMvc) {
        MockMvcUtil.mockMvc = mockMvc;
    }
}
