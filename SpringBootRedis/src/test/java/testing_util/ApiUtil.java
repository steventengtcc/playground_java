package testing_util;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import org.example.APIPathConstant;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;

public class ApiUtil {

    private static final MockMvc mockMvc = MockMvcUtil.getMockMvc();

    private ApiUtil() {
    }

    public static String loadCache(String key, ResultMatcher... resultMatchers) throws Exception {
        return mockMvc.perform(get(APIPathConstant.CACHE + "/" + key))
                .andExpectAll(resultMatchers)
                .andReturn()
                .getResponse()
                .getContentAsString();
    }

    public static String updateCache(String key, ResultMatcher... resultMatchers) throws Exception {
        return mockMvc.perform(put(APIPathConstant.CACHE + "/" + key))
                .andExpectAll(resultMatchers)
                .andReturn()
                .getResponse()
                .getContentAsString();
    }

    public static void deleteCache(String key, ResultMatcher... resultMatchers) throws Exception {
        mockMvc.perform(delete(APIPathConstant.CACHE + "/" + key))
                .andExpectAll(resultMatchers);
    }

    public static void deleteCache(ResultMatcher... resultMatchers) throws Exception {
        mockMvc.perform(delete(APIPathConstant.CACHE))
                .andExpectAll(resultMatchers);
    }

    public static void deleteCacheByCacheManager(String key, ResultMatcher... resultMatchers) throws Exception {
        mockMvc.perform(delete(APIPathConstant.CACHE + APIPathConstant.CACHE_MANAGER + "/" + key))
                .andExpectAll(resultMatchers);
    }

    public static void deleteCacheByCacheManager(ResultMatcher... resultMatchers) throws Exception {
        mockMvc.perform(delete(APIPathConstant.CACHE + APIPathConstant.CACHE_MANAGER))
                .andExpectAll(resultMatchers);
    }

    public static String loadCacheByRedisTemplate(String key, ResultMatcher... resultMatchers) throws Exception {
        return mockMvc.perform(get(APIPathConstant.CACHE + APIPathConstant.REDIS_TEMPLATE + "/" + key))
                .andExpectAll(resultMatchers)
                .andReturn()
                .getResponse()
                .getContentAsString();
    }

    public static String updateCacheByRedisTemplate(String key, ResultMatcher... resultMatchers) throws Exception {
        return mockMvc.perform(put(APIPathConstant.CACHE + APIPathConstant.REDIS_TEMPLATE + "/" + key))
                .andExpectAll(resultMatchers)
                .andReturn()
                .getResponse()
                .getContentAsString();
    }

    public static void deleteCacheByRedisTemplate(String key, ResultMatcher... resultMatchers) throws Exception {
        mockMvc.perform(delete(APIPathConstant.CACHE + APIPathConstant.REDIS_TEMPLATE + "/" + key))
                .andExpectAll(resultMatchers);
    }

    public static void deleteCacheByRedisTemplate(ResultMatcher... resultMatchers) throws Exception {
        mockMvc.perform(delete(APIPathConstant.CACHE + APIPathConstant.REDIS_TEMPLATE))
                .andExpectAll(resultMatchers);
    }
}
