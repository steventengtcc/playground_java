# SpringBoot Redis Sample Code

## Prerequisites

- Spring Boot 3
- Redis 7.2.6
- Java 18
- Maven 3.9.8
- Docker Desktop

## Run Redis Server

Make sure you have Docker Desktop installed on your machine. Then run the following command to start the Redis server.

```shell
./run-redis.sh up
```

To stop the Redis server, run the following command.

```shell
./run-redis.sh down
```
