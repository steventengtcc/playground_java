package org.example.first;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
public class FirstService {

    private final ConcurrentHashMap<String, First> mockDB = new ConcurrentHashMap<>();

    public First createItem(First first) {
        final var newItem = new First();
        BeanUtils.copyProperties(first, newItem, "id");
        newItem.setId(String.valueOf(System.currentTimeMillis()));

        mockDB.put(newItem.getId(), newItem);
        return newItem;
    }

    public First getItem(String id) {
        return Optional.ofNullable(mockDB.get(id)).orElseThrow();
    }
}
