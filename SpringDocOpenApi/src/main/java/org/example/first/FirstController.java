package org.example.first;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping(value = "/item")
@Tag(name = "FirstApi")
public class FirstController {

    @Autowired
    private FirstService firstService;

    @Operation(summary = "Create document.")
    @PostMapping
    public First createItem(@RequestBody First first) {
        return firstService.createItem(first);
    }

    @Operation(summary = "Load document.")
    @GetMapping(value = "/{id}")
    public First loadById(@PathVariable(name = "id") String id) {
        return firstService.getItem(id);
    }
}
