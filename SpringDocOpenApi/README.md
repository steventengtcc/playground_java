# OpenAPI 3 - SpringDoc Sample Code

## Prerequisites

* Spring Boot 3
* Springdoc OpenAPI 2.6
* Java 18
* Maven 3.9.8
