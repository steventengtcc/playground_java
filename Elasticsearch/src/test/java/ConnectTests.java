import org.example.Main;
import org.example.SampleData;
import org.example.SampleDataRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@ExtendWith(SpringExtension.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = Main.class)
public class ConnectTests {

    @Autowired
    private SampleDataRepository sampleDataRepository;

    @Test
    public void testConnectES() {
        var data = SampleData.of("test");
        sampleDataRepository.save(data);
        assertNotNull(data.getId());

        var result = sampleDataRepository.findById(data.getId()).orElseThrow();
        assertEquals(data.getId(), result.getId());
        assertEquals(data.getName(), result.getName());
    }

}
