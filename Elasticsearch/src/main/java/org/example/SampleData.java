package org.example;

import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "sample_data")
public class SampleData {

    private String id;

    private String name;

    public static SampleData of(String name) {
        var data = new SampleData();
        data.setName(name);
        return data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
