package org.example;

import java.io.File;

public interface IVideoService {
    byte[] genThumbnail(File file);
}
