package org.example;

import java.io.ByteArrayOutputStream;
import java.io.File;

import javax.imageio.ImageIO;

import org.jcodec.api.FrameGrab;
import org.jcodec.scale.AWTUtil;
import org.springframework.stereotype.Service;

@Service
public class JCodecVideoService implements IVideoService {

    @Override
    public byte[] genThumbnail(File file) {
        try {
            final var picture = FrameGrab.getFrameFromFile(file, 0);
            final var baos = new ByteArrayOutputStream();
            ImageIO.write(AWTUtil.toBufferedImage(picture), "jpg", baos);
            return baos.toByteArray();
        } catch (Exception e) {
            throw new RuntimeException("Failed to get frame", e);
        }
    }
}
