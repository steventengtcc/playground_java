package org.example;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.springframework.stereotype.Service;

@Service
public class JavaCVVideoService implements IVideoService {

    @Override
    public byte[] genThumbnail(File file) {
        try {
            return genThumbnail(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public byte[] genThumbnail(InputStream inputStream) {
        try (final var grabber = new FFmpegFrameGrabber(inputStream);
             final var converter = new Java2DFrameConverter()) {
            grabber.start();

            final var frame = grabber.grabKeyFrame();
            final var baos = new ByteArrayOutputStream();
            ImageIO.write(converter.getBufferedImage(frame), "jpg", baos);
            return baos.toByteArray();
        } catch (Exception e) {
            throw new RuntimeException("Failed to save file", e);
        }
    }
}
