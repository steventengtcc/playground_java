package org.example;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class VideoServiceTest {

    /**
     * Path to the video file.
     * Change this to the path of a video file on your machine.
     */
    private static final String FILE_PATH = "src/test/resources/video.mp4";
    @Autowired
    private JavaCVVideoService javaCVVideoService;
    @Autowired
    private JCodecVideoService jCodecVideoService;

    @Test
    void testJavaCVVideoService() {
        final var file = new File(FILE_PATH);
        final var thumbnail = javaCVVideoService.genThumbnail(file);
        assertNotNull(thumbnail);
        assertTrue(thumbnail.length > 0);

        saveThumbnail(thumbnail);
    }

    @Test
    void testJCodecVideoService() {
        final var file = new File(FILE_PATH);
        final var thumbnail = jCodecVideoService.genThumbnail(file);
        assertNotNull(thumbnail);
        assertTrue(thumbnail.length > 0);

        saveThumbnail(thumbnail);
    }

    private void saveThumbnail(byte[] thumbnail) {
        try {
            BufferedImage img = ImageIO.read(new ByteArrayInputStream(thumbnail));
            ImageIO.write(img, "jpg", new File("src/test/resources/thumbnail.jpg"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}