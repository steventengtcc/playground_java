# Java Generate Video Thumbnail Sample Code

This code sample demonstrates how to generate a thumbnail image from a video file using the Java programming language.
[My Blog](https://steventengtcc.gitlab.io/website/2025/02/13/java-generate-video-thumbnail/)

## Prerequisites

* Spring Boot 3
* Java 21
* Maven 3.9.8
* JavaCV
* JCodec

## Video

Place target video file in the resources folder.

```shell
./JavaGenerateVideoThumbnail/src/test/resources/video.mp4
```
