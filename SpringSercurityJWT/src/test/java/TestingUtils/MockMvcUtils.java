package TestingUtils;

import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;

import java.io.UnsupportedEncodingException;

public class MockMvcUtils {
    private MockMvcUtils() {
    }

    public static ResultActions andExpect(ResultActions resultActions, ResultMatcher... matchers) throws Exception {
        var actions = resultActions;
        for (var matcher : matchers) {
            actions = actions.andExpect(matcher);
        }
        return actions;
    }

    public static String getContentAsString(MvcResult result) throws UnsupportedEncodingException {
        return result.getResponse().getContentAsString();
    }
}
