package TestingHelper;

import TestingUtils.MockMvcUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.constants.OAuth2Constants;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Map;

public class LoginApiHelper {

    public static final String LOGIN_PATH = "/login";

    private final MockMvc mockMvc;

    private final ObjectMapper objectMapper = new ObjectMapper();

    public LoginApiHelper(MockMvc mockMvc) {
        this.mockMvc = mockMvc;
    }

    public Map<String, Object> login(String username, String password, ResultMatcher... matchers) throws Exception {
        var contentString = objectMapper.writeValueAsString(
                Map.of(OAuth2Constants.GRANT_TYPE, OAuth2Constants.PASSWORD,
                        OAuth2Constants.USERNAME, username,
                        OAuth2Constants.PASSWORD, password)
        );
        var requestBuilder = MockMvcRequestBuilders.post(LOGIN_PATH)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(contentString);
        var mvcResult = MockMvcUtils.andExpect(mockMvc.perform(requestBuilder), matchers)
                .andReturn();
        return objectMapper.readValue(MockMvcUtils.getContentAsString(mvcResult), new TypeReference<>() {
        });
    }

    public Map<String, Object> refreshToken(String refreshToken, ResultMatcher... matchers) throws Exception {
        var contentString = objectMapper.writeValueAsString(
                Map.of(OAuth2Constants.GRANT_TYPE, OAuth2Constants.REFRESH_TOKEN,
                        OAuth2Constants.REFRESH_TOKEN, refreshToken)
        );
        var requestBuilder = MockMvcRequestBuilders.post(LOGIN_PATH)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(contentString);
        var mvcResult = MockMvcUtils.andExpect(mockMvc.perform(requestBuilder), matchers)
                .andReturn();
        return objectMapper.readValue(MockMvcUtils.getContentAsString(mvcResult), new TypeReference<>() {
        });
    }
}
