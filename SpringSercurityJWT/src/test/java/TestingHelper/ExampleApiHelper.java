package TestingHelper;

import TestingConfigurer.TokenConfigurer;
import TestingUtils.MockMvcUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

public class ExampleApiHelper {

    public static final String EXAMPLE_PATH = "/example";

    private final MockMvc mockMvc;
    private final TokenConfigurer tokenConfigurer;

    private final ObjectMapper objectMapper = new ObjectMapper();

    public ExampleApiHelper(MockMvc mockMvc, TokenConfigurer tokenConfigurer) {
        this.mockMvc = mockMvc;
        this.tokenConfigurer = tokenConfigurer;
    }

    public String loadExampleById(String id, ResultMatcher... matchers) throws Exception {
        var requestBuilder = MockMvcRequestBuilders.get(EXAMPLE_PATH + "/" + id)
                .header("Authorization", tokenConfigurer.getAccessToken());
        var mvcResult = MockMvcUtils.andExpect(mockMvc.perform(requestBuilder), matchers)
                .andReturn();
        return MockMvcUtils.getContentAsString(mvcResult);
    }

    public List<String> listingExample(ResultMatcher... matchers) throws Exception {
        var requestBuilder = MockMvcRequestBuilders.get(EXAMPLE_PATH)
                .header("Authorization", tokenConfigurer.getAccessToken());
        var mvcResult = MockMvcUtils.andExpect(mockMvc.perform(requestBuilder), matchers)
                .andReturn();
        return objectMapper.readValue(MockMvcUtils.getContentAsString(mvcResult), new TypeReference<>() {
        });
    }
}
