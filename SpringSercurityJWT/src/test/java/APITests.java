import TestingConfigurer.TokenConfigurer;
import TestingHelper.ExampleApiHelper;
import TestingHelper.LoginApiHelper;
import org.example.Main;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = Main.class
)
@AutoConfigureMockMvc
class APITests {

    private TokenConfigurer tokenConfigurer;

    private LoginApiHelper loginApiHelper;

    private ExampleApiHelper exampleApiHelper;

    @Autowired
    public APITests(MockMvc mockMvc) throws Exception {
        this.loginApiHelper = new LoginApiHelper(mockMvc);
        initLogin(loginApiHelper);
        this.exampleApiHelper = new ExampleApiHelper(mockMvc, tokenConfigurer);
    }

    public void initLogin(LoginApiHelper loginApiHelper) throws Exception {
        var result = loginApiHelper.login("admin", "pass", status().isOk());
        this.tokenConfigurer = new TokenConfigurer((String) result.get("access_token"), (String) result.get("refresh_token"));
    }

    @Test
    void testLoginApi() throws Exception {
        var result = loginApiHelper.login("admin", "pass", status().isOk(),
                jsonPath("$.access_token").exists(),
                jsonPath("$.refresh_token").exists(),
                jsonPath("$.token_type").value("Bearer"),
                jsonPath("$.expires_in").exists());
        System.out.println(result);
    }

    @Test
    void testRefreshToken() throws Exception {
        var refreshToken = (String) loginApiHelper.login("admin", "pass", status().isOk()).get("refresh_token");
        var result = loginApiHelper.refreshToken(refreshToken, status().isOk(),
                jsonPath("$.access_token").exists(),
                jsonPath("$.refresh_token").exists(),
                jsonPath("$.token_type").value("Bearer"),
                jsonPath("$.expires_in").value(60));
        System.out.println(result);
    }

    @Test
    void testLoadExampleById() throws Exception {
        var result = exampleApiHelper.loadExampleById("123", status().isOk());
        Assertions.assertEquals("Hello Example: 123", result);
    }

    @Test
    void testListingExample() throws Exception {
        exampleApiHelper.listingExample(status().isOk(),
                jsonPath("$.length()").value(2),
                jsonPath("$[0]").value("Hello Example: "),
                jsonPath("$[1]").value("123"));
    }
}
