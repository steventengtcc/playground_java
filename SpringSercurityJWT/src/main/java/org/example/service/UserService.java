package org.example.service;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class UserService implements UserDetailsService {

    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        if (!username.equalsIgnoreCase("admin")) {
            throw new UsernameNotFoundException(username);
        }
        return (User) User.builder().username("admin").password("{noop}pass").build();
    }
}
