package org.example.service;

import org.example.constants.OAuth2Constants;
import org.example.exception.BadRequestException;
import org.example.utils.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class LoginService {

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    public Map<String, Object> login(Map<String, String> request) {
        var grantType = request.get(OAuth2Constants.GRANT_TYPE);
        if (OAuth2Constants.PASSWORD.equals(grantType)) {
            return loginByPassword(request);
        } else if (OAuth2Constants.REFRESH_TOKEN.equals(grantType)) {
            return refreshToken(request.get(OAuth2Constants.REFRESH_TOKEN));
        }
        throw new BadRequestException("Unknown grantType: " + grantType);
    }

    public Map<String, Object> loginByPassword(Map<String, String> request) {
        var username = request.get(OAuth2Constants.USERNAME);
        var password = request.get(OAuth2Constants.PASSWORD);

        var authToken = new UsernamePasswordAuthenticationToken(username, password);
        // throw BadCredentialsException in authenticationManager
        var authentication = authenticationManager.authenticate(authToken);

        var user = (User) authentication.getPrincipal();
        return Map.of(
                OAuth2Constants.ACCESS_TOKEN, JwtUtils.generateToken(user),
                OAuth2Constants.TOKEN_TYPE, OAuth2Constants.BEARER,
                OAuth2Constants.EXPIRES_IN, JwtUtils.TOKEN_EXPIRED_TIME,
                OAuth2Constants.REFRESH_TOKEN, JwtUtils.generateRefreshToken(user)
        );
    }

    public Map<String, Object> refreshToken(String refreshToken) {
        var username = JwtUtils.parseRefreshToken(refreshToken);
        var user = userService.loadUserByUsername(username);
        return Map.of(
                OAuth2Constants.ACCESS_TOKEN, JwtUtils.generateToken(user),
                OAuth2Constants.TOKEN_TYPE, OAuth2Constants.BEARER,
                OAuth2Constants.EXPIRES_IN, JwtUtils.TOKEN_EXPIRED_TIME,
                OAuth2Constants.REFRESH_TOKEN, refreshToken
        );
    }
}
