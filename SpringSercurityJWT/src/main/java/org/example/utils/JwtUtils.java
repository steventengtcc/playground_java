package org.example.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.example.exception.TokenExpiredException;
import org.springframework.security.core.userdetails.User;

import javax.crypto.SecretKey;
import java.security.Key;
import java.util.Calendar;
import java.util.Map;

public class JwtUtils {

    public static final int TOKEN_EXPIRED_TIME = 60;
    public static final int REFRESH_TOKEN_EXPIRED_TIME = 60 * 120;

    private static final String ISS = "AuthServer";
    private static final String TOKEN_SECRET = "longlongsecretlonglongsecretlonglongsecretlonglongsecretlonglongsecretlonglongsecret";
    private static final String REFRESH_TOKEN_SECRET = "refreshlonglongsecretlonglongsecretlonglongsecretlonglongsecretlonglongsecretlonglongsecret";

    private JwtUtils() {
    }

    public static String generateToken(User user) {
        Calendar exp = Calendar.getInstance();
        exp.add(Calendar.SECOND, TOKEN_EXPIRED_TIME);

        var mapper = new ObjectMapper();
        Key secretKey = Keys.hmacShaKeyFor(Decoders.BASE64.decode(TOKEN_SECRET));
        return Jwts.builder()
                .header()
                .type("JWT")
                .and()
                .subject(user.getUsername())
                .expiration(exp.getTime())
                .issuer(ISS)
                .claims(mapper.convertValue(user, new TypeReference<Map<String, Object>>() {
                }))
                .signWith(secretKey)
                .compact();
    }

    public static String generateRefreshToken(User user) {
        var expireDate = Calendar.getInstance();
        expireDate.add(Calendar.SECOND, REFRESH_TOKEN_EXPIRED_TIME);

        Key secretKey = Keys.hmacShaKeyFor(Decoders.BASE64.decode(REFRESH_TOKEN_SECRET));
        return Jwts.builder()
                .header()
                .type("JWT")
                .and()
                .subject(user.getUsername())
                .expiration(expireDate.getTime())
                .issuer(ISS)
                .signWith(secretKey)
                .compact();
    }

    public static String parseAccessToken(String token) {
        return parseToken(token, Keys.hmacShaKeyFor(Decoders.BASE64.decode(TOKEN_SECRET)));
    }

    public static String parseRefreshToken(String token) {
        return parseToken(token, Keys.hmacShaKeyFor(Decoders.BASE64.decode(REFRESH_TOKEN_SECRET)));
    }

    public static String parseToken(String token, SecretKey key) {
        var parser = Jwts.parser()
                .verifyWith(key)
                .build();
        try {
            var claims = parser.parseSignedClaims(token).getPayload();
            return claims.getSubject();
        } catch (ExpiredJwtException exception) {
            throw new TokenExpiredException(exception);
        }
    }
}
