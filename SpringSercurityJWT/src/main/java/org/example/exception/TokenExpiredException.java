package org.example.exception;

public class TokenExpiredException extends RuntimeException {

    public TokenExpiredException(Throwable throwable) {
        super("Token has expired!", throwable);
    }
}
