package org.example.constants;

public class OAuth2Constants {

    public static final String GRANT_TYPE = "grant_type";
    public static final String PASSWORD = "password";
    public static final String REFRESH_TOKEN = "refresh_token";
    public static final String USERNAME = "username";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String TOKEN_TYPE = "token_type";
    public static final String EXPIRES_IN = "expires_in";
    public static final String BEARER = "Bearer";

    private OAuth2Constants() {
    }


}
