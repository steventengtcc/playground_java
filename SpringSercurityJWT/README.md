## Playground of Spring Security & JWT

### Target

- [x] Use Spring Security to protect api endpoint
- [x] Authorization with JWT token
- [x] Access token & Refresh token
- [x] Reading OAuth standard

## Q&A

### Q1

How to throw explicit exception to api caller?  
Why response body is empty and status code is 403 when exception occur?  
**Root Cause**

1. Tomcat will redirect to error page when exception occur. (default path "/error")
2. Spring Security default to protect all endpoint.   
   Which lead to 403 when tomcat redirect to error page without authorization.

**Solution:**

```java

@Configuration
@EnableWebSecurity
public class DefaultSecurityConfig {
    @Bean
    @Order(0)
    public SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) {
        http.authorizeHttpRequests(registry ->
                // permit request with pattern "/error"
                registry.requestMatchers("/error").permitAll());
        return http.build();
    }
}
```

**Reference:**  
[Spring Security - Unhandled exception returns 403 instead of 500](https://stackoverflow.com/questions/76645593/spring-security-unhandled-exception-returns-403-instead-of-500)

### Q2

Why Spring security always returns HTTP 403 with POST method.

**Root Cause**

1. Spring Security enable csrf protection with default.   
   Which ask api caller bring a specific **_csrf** in request body of POST api.

**Solution:**

```java

@Configuration
@EnableWebSecurity
public class DefaultSecurityConfig {
    @Bean
    @Order(0)
    public SecurityFilterChain defaultFilterChain(HttpSecurity http) {
        // disable csrf protection
        http.csrf(AbstractHttpConfigurer::disable);
        return http.build();
    }
}
```

**Reference:**   
[How to Solve 403 Error in Spring Boot POST Request](https://www.baeldung.com/java-spring-fix-403-error)  
[Spring Security configuration: HTTP 403 error](https://stackoverflow.com/questions/19468209/spring-security-configuration-http-403-error)
