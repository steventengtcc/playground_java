import org.example.Main;
import org.example.MongoDocument;
import org.example.MongoDocumentRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = Main.class)
public class ConnectTest {

    @Autowired
    private MongoDocumentRepository mongoDocumentRepository;

    @Test
    public void testConnectMongo() {
        var document = MongoDocument.of("test");
        mongoDocumentRepository.save(document);
        assertNotNull(document.getId());

        var result = mongoDocumentRepository.findById(document.getId()).orElseThrow();
        assertEquals(document.getId(), result.getId());
        assertEquals(document.getName(), result.getName());
    }
}
