package org.example;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SampleDataRepository extends ElasticsearchRepository<SampleData, String> {
}
