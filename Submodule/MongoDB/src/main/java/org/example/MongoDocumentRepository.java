package org.example;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MongoDocumentRepository extends MongoRepository<MongoDocument, String> {
}
