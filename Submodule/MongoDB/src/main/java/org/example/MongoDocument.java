package org.example;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(value = "mongo_document")
public class MongoDocument {

    private String id;

    private String name;

    public static MongoDocument of(String name) {
        var document = new MongoDocument();
        document.setName(name);
        return document;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
