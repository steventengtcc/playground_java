## Background

This project build a OAuth2 server by below plugin.

* spring-boot-starter-oauth2-authorization-server
* spring-boot-starter-oauth2-resource-server

**oauth2-authorization-server** default support OAuth2 flow, **authorization code**.  
OAuth2 flow **password** is deprecated and not support by **oauth2-authorization-server**.  
This project is trying to support OAuth2 flow **password** base on **oauth2-authorization-server**.

## Note

1. **oauth2-authorization-server** base on **spring-security**, add some configuer (
   ex. `OAuth2AuthorizationServerConfigurer`)
2. **oauth2-resource-server** base on **spring-security**, add some configuer (ex. `OAuth2ResourceServerConfigurer`)
3. **oauth2-authorization-server** leave an interface `OAuth2AuthorizationServerConfigurer.tokenEndpoint()` to add self
   defined converter.  
   I add self defined AuthenticationConverter `OAuth2PasswordAuthenticationConverter` which extract login info from
   request body.  
   And self defined AuthenticationProvider `OAuth2PasswordAuthenticationProvider` which check login info is correct and
   authenticate.  
   `OAuth2PasswordAuthenticationConverter` and `OAuth2PasswordAuthenticationProvider` will call
   by `OAuth2TokenEndpointFilter`. 




