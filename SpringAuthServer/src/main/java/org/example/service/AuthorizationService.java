package org.example.service;

import org.springframework.security.oauth2.server.authorization.InMemoryOAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.OAuth2Authorization;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.OAuth2TokenType;
import org.springframework.stereotype.Service;

@Service
public class AuthorizationService implements OAuth2AuthorizationService {

    private InMemoryOAuth2AuthorizationService oAuth2AuthorizationService = new InMemoryOAuth2AuthorizationService();

    @Override
    public void save(OAuth2Authorization authorization) {
        oAuth2AuthorizationService.save(authorization);
    }

    @Override
    public void remove(OAuth2Authorization authorization) {
        oAuth2AuthorizationService.remove(authorization);
    }

    @Override
    public OAuth2Authorization findById(String id) {
        return oAuth2AuthorizationService.findById(id);
    }

    @Override
    public OAuth2Authorization findByToken(String token, OAuth2TokenType tokenType) {
        return oAuth2AuthorizationService.findByToken(token, tokenType);
    }
}
