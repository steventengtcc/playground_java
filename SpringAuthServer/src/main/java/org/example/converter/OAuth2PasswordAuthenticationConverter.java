package org.example.converter;

import jakarta.servlet.http.HttpServletRequest;
import org.example.constatnts.OAuth2Constants;
import org.example.entity.OAuth2PasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.web.authentication.AuthenticationConverter;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OAuth2PasswordAuthenticationConverter implements AuthenticationConverter {
    @Override
    public Authentication convert(HttpServletRequest request) {
        var grantType = request.getParameter(OAuth2ParameterNames.GRANT_TYPE);
        if (!OAuth2Constants.PASSWORD.equals(grantType)) {
            return null;
        }

        var clientAuthentication = SecurityContextHolder.getContext().getAuthentication();
        return new OAuth2PasswordAuthenticationToken(clientAuthentication, getAdditionalParameters(request));
    }

    public Map<String, Object> getAdditionalParameters(HttpServletRequest request) {
        return Stream.of(OAuth2Constants.USERNAME, OAuth2Constants.PASSWORD)
                .map(key -> Map.entry(key, request.getParameter(key)))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
}
