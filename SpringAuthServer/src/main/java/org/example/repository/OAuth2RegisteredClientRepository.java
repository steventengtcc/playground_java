package org.example.repository;

import org.example.exception.NotFoundException;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.stereotype.Repository;

@Repository
public class OAuth2RegisteredClientRepository implements RegisteredClientRepository {

    @Override
    public void save(RegisteredClient registeredClient) {
        // Currently not support
    }

    @Override
    public RegisteredClient findById(String id) {
        if (!"id".equals(id)) {
            throw new NotFoundException("Can not found by id: " + id);
        }
        return genRegisteredClient();
    }

    @Override
    public RegisteredClient findByClientId(String clientId) {
        if (!"clientId".equals(clientId)) {
            throw new NotFoundException("Can not found by clientId: " + clientId);
        }
        return genRegisteredClient();
    }

    public RegisteredClient genRegisteredClient() {
        return RegisteredClient.withId("id")
                .clientId("clientId")
                .clientSecret("{noop}secret")
                .authorizationGrantType(AuthorizationGrantType.PASSWORD)
                .authorizationGrantType(AuthorizationGrantType.REFRESH_TOKEN)
                .build();
    }
}
