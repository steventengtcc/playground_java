package org.example.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/example")
public class ExampleController {

    @GetMapping(value = "/{id}")
    public ResponseEntity<String> loadExample(@PathVariable("id") String id) {
        return ResponseEntity.ok("Hello example id: " + id);
    }

    @GetMapping
    public ResponseEntity<List<Map<String, Object>>> listExample() {
        return ResponseEntity.ok(List.of(Map.of("id", 123, "name", "abc")));
    }
}
