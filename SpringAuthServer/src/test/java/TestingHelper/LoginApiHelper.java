package TestingHelper;

import TestingUtils.MockMvcUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.constatnts.OAuth2Constants;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Map;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;

public class LoginApiHelper {

    public static final String LOGIN_PATH = "/oauth2/token";

    private final MockMvc mockMvc;

    private final ObjectMapper objectMapper = new ObjectMapper();

    public LoginApiHelper(MockMvc mockMvc) {
        this.mockMvc = mockMvc;
    }

    public Map<String, Object> login(String username, String password, ResultMatcher... matchers) throws Exception {
        var requestBuilder = MockMvcRequestBuilders.post(LOGIN_PATH)
                .with(httpBasic("clientId", "secret"))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .param(OAuth2ParameterNames.GRANT_TYPE, AuthorizationGrantType.PASSWORD.getValue())
                .param(OAuth2Constants.USERNAME, username)
                .param(OAuth2Constants.PASSWORD, password);
        var mvcResult = MockMvcUtils.andExpect(mockMvc.perform(requestBuilder), matchers)
                .andReturn();
        return objectMapper.readValue(MockMvcUtils.getContentAsString(mvcResult), new TypeReference<>() {
        });
    }

    public Map<String, Object> refreshToken(String refreshToken, ResultMatcher... matchers) throws Exception {
        var requestBuilder = MockMvcRequestBuilders.post(LOGIN_PATH)
                .with(httpBasic("clientId", "secret"))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .param(OAuth2ParameterNames.GRANT_TYPE, AuthorizationGrantType.REFRESH_TOKEN.getValue())
                .param(OAuth2ParameterNames.REFRESH_TOKEN, refreshToken);
        var mvcResult = MockMvcUtils.andExpect(mockMvc.perform(requestBuilder), matchers)
                .andReturn();
        return objectMapper.readValue(MockMvcUtils.getContentAsString(mvcResult), new TypeReference<>() {
        });
    }
}
